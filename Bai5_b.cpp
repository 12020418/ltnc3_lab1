// full name: Dao Thi Tuyen
//lop		: 	K57D
// chuong trinh fib su dung de qui
// bai 5- tuan 1

#include<iostream>
#include<windows.h>
#include<iomanip>
using namespace std;

int A[100];
	int k, i;

int fibo(int k) {
	int kq;
	if (k == 1) kq = 0;
	if (k == 2) kq = 1;
	if (k > 2) kq = fibo(k-1) + fibo(k-2);
	return kq;
}

int main() {
	cout << " ban muon tim so fibonanci thu : "; cin >> k;
	cout << "so fib thu " << k << " la : " << fibo(k) << endl;
	system("pause");
}
